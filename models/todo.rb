class Todo
  include Mongoid::Document

  field :title, type: String
  field :completed, type: Boolean
end