require 'sinatra/base'
require 'sinatra/reloader'
require 'slim'
require 'mongoid'
require './models/todo.rb'
require 'json'

class App < Sinatra::Base
  configure :development do
    register Sinatra::Reloader
  end

  # mongoid
  configure do
    Mongoid.load!("./mongoid.yml")
  end

  # 静的リソース
  set :public_folder, File.dirname(__FILE__) + '/assets'

  get '/' do
    slim :index
  end

  get '/todo' do
    todo = Todo.all
    todo.to_json
  end

  put '/todo/:id' do
    body = JSON.parse request.body.read, {:symbolize_names => true}
    body.delete :_id #_idは更新できないので除去
    id = params[:id]
    todo = Todo.where(_id: id)
      .find_and_modify({ '$set' => body }, new: true)
    todo.to_json
  end

  delete '/todo/:id' do
    id = params[:id]
    todo = Todo.where(_id: id).first
    todo.destroy
  end

  post '/todo' do
    # bodyをparseする。その時keyをSymbolにする
    params = JSON.parse request.body.read, {:symbolize_names => true}
    todo = Todo.new
    todo.title = params[:title]
    todo.completed = false
    todo.save
  end

end
