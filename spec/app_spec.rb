ENV['RACK_ENV'] = 'test'

require './app.rb'
require 'rspec'
require 'rack/test'
require 'database_cleaner'

RSpec.configure do |config|

  config.before :all do
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.start
  end

  # テスト後にDBをクリア
  config.after :all do
    DatabaseCleaner.clean
  end
end

describe 'App behaviour' do
  include Rack::Test::Methods

  def app
    App
  end

  # テストデータを作成
  before :all do
    todo = Todo.new
    todo.title = 'test@@@'
    todo.completed = false
    todo.save
  end

  # indexページが表示されること
  it 'GET on index' do
    get '/'
    last_response.should be_ok
    last_response.body.should include '<title>Todo</title>'
  end

  # todoデータがjsonで取得できる
  it 'GET on /todo' do
    get '/todo'
    res = JSON.parse last_response.body
    res.size.should be 1
    res[0]['title'].should eq 'test@@@'
  end

  # todoデータが登録できる
  it 'POST on /todo' do
    post '/todo',{
      title: 'test2@@@',
    }.to_json
    # 登録されている
    Todo.all.size.should be 2
  end

  # todoデータが更新できる
  it 'POST to /todo/:id' do
    todo = Todo.all.first
    id = todo._id
    put "/todo/#{id}", {
      title: 'test@@@_update',
      completed: true
    }.to_json
    new_todo = Todo.where(_id: id).first
    new_todo.title.should eq 'test@@@_update'
    new_todo.completed.should be true
  end

  # todoデータが削除できる
  it 'DELETE to /todo/:id' do
    todo = Todo.all.first
    id = todo._id
    delete "/todo/#{id}"
    Todo.all.size.should be 1
  end
end